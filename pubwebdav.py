#!/usr/bin/env python3

## Copyright 2023 Lasse Kliemann <lasse@lassekliemann.de>
## MIT License
## https://gitlab.com/lxkl/pubtools

import argparse, os, subprocess, sys, yaml
from datetime import datetime
from webdav3.client import Client
from webdav3.exceptions import WebDavException

# Read whole file into string.
def slurp_file_(path_):
	with open(path_, 'r') as fo_: str_ = fo_.read()
	return str_

# Get extension from filename.
def get_ext_(path_):
	return os.path.splitext(path_)[1]

# Remove extension (including dot) and add ext_ instead.
def replace_ext_(str_, ext_):
	return os.path.splitext(str_)[0] + ext_

# Add extension designating file containing signature; check if file
# has such signature.
def add_sign_ext_(path_):
	return path_ + '.sig'
def has_sign_ext_(path_):
	return get_ext_(path_) == '.sig'

# Check if given git repos is in clean state.
def is_clean_repos_(repos_path_):
	if not os.path.isdir(repos_path_):
		return False
	out_ = subprocess.check_output(['git', 'status', '--porcelain'], cwd=repos_path_).decode('utf-8').strip()
	return out_ == ''

# Try to build PDF file file_ in dir_path_ using latexmmk. Some
# details are specific to my latexmk configuration, which can be found
# in the LatexMk file in this repos for convenience.
def build_pdf_file_(dir_path_, file_):
	env_ = os.environ.copy()
	env_['halt_'] = ''
	print('building, if necessary: {}... '.format(file_), end='', flush=True)
	latexmk_ = subprocess.run(['latexmk', replace_ext_(file_, '.tex')], cwd=dir_path_, env=env_,
							  stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
	path_ = os.path.join(dir_path_, '.latexmk.out_dir', file_)
	if os.path.isfile(path_) and (latexmk_.returncode == 0):
		print('success')
		return path_
	else:
		print('FAILED')
		return False

# Just see if the file exists, and if so, return its path.
def find_file_(dir_path_, file_):
	print('finding: {}... '.format(file_), end='', flush=True)
	path_ = os.path.join(dir_path_, file_)
	if os.path.isfile(path_):
		print('found')
		return path_
	else:
		print('NOT FOUND')
		return False

# Make signature using qubes-gpg-client and the default key.
def make_signature_(path_):
	sign_path_ = add_sign_ext_(path_)
	if os.path.isfile(sign_path_) and os.stat(sign_path_).st_mtime <= os.stat(path_).st_mtime:
		os.remove(sign_path_)
	if not os.path.isfile(sign_path_):
		with open(sign_path_, 'w') as fo_:
			subprocess.run(['qubes-gpg-client', '--armor', '--detach-sign', path_], stdout=fo_)
	return sign_path_

# Take a particularly formatted timestamp str_ and return the number
# of seconds since January 1, 1970 up to the point in time given by
# str_.
def string_to_epoch_(str_):
	fmt_ = '%a, %d %b %Y %H:%M:%S %Z'
	epoch_ = datetime(1970, 1, 1)
	return (datetime.strptime(str_, fmt_) - epoch_).total_seconds()

# Using webdav connection given via client_, upload file given by
# local_path_ to remote_path_, if the latter does not yet exist or if
# the local file is newer.
def upload_file_if_newer_(client_, local_path_, remote_path_):
	check_ = client_.check(remote_path_)
	if check_:
		local_mtime_ = os.stat(local_path_).st_mtime
		remote_mtime_ = string_to_epoch_(client_.info(remote_path_)['modified'])
	if (not check_) or (local_mtime_ >= remote_mtime_):
		print('uploading: {} --> {}'.format(os.path.basename(local_path_), os.path.basename(remote_path_)))
		client_.upload_sync(local_path=local_path_, remote_path=remote_path_)

# Log into the webdav account, then iterate over the list of remote
# directories for this account. For each remote directory, iterate
# over the list of local directories (should be git repositories), and
# for each local directory over a list of files. For each file, build
# it if necessary and then upload using upload_file_if_newer_.
def process_account_(account_):
	print('====> {}'.format(account_['host']))
	warn_fail_count_ = 0
	try:
		client_ = Client({'webdav_hostname': account_['host'],
						  'webdav_login': account_['login'],
						  'webdav_password': account_['password']})
		for remote_ in account_['remotes']:
			remote_dir_path_ = remote_['dir']
			print('==> {}'.format(remote_dir_path_))
			if not remote_['update']:
				print('not marked for update; skipping')
				continue
			if not client_.check(remote_dir_path_):
				print('WARNING: skipping missing remote directory')
				warn_fail_count_ += 1
				continue
			keep_file_list_ = []
			for local_ in remote_['locals']:
				local_dir_path_ = os.path.expanduser(local_['dir'])
				print('=> {}'.format(local_dir_path_))
				if not is_clean_repos_(local_dir_path_):
					print('WARNING: skipping: not a clean repos')
					warn_fail_count_ += 1
					continue
				if ARGS_.c:
					subprocess.run(['git', 'clean', '-xdf'], cwd=local_dir_path_)
				branch_ = local_.get('branch')
				if branch_ is not None:
					for cmd_ in [['git', 'checkout', branch_],
								 ['git', 'pull']]:
						subprocess.run(cmd_, cwd=local_dir_path_)
				for file_ in (local_['files'] or []):
					y_ = [x_.strip() for x_ in file_.split(' --> ')]
					if len(y_) == 1:
						local_file_ = y_[0]
						remote_file_ = y_[0]
					elif len(y_) == 2:
						local_file_ = y_[0]
						remote_file_ = y_[1]
					else:
						print('WARNING: skipping file: {}'.format(file_))
						warn_fail_count_ += 1
						continue
					keep_file_list_ += [remote_file_]
					if get_ext_(local_file_) == '.pdf':
						local_path_ = build_pdf_file_(local_dir_path_, local_file_)
					else:
						local_path_ = find_file_(local_dir_path_, local_file_)
					if local_path_:
						upload_file_if_newer_(client_, local_path_, os.path.join(remote_dir_path_, remote_file_))
						if not has_sign_ext_(local_file_) and ARGS_.s:
							keep_file_list_ += [add_sign_ext_(remote_file_)]
							local_sign_path_ = make_signature_(local_path_)
							upload_file_if_newer_(client_, local_sign_path_, add_sign_ext_(os.path.join(remote_dir_path_, remote_file_)))
					else:
						warn_fail_count_ += 1
			if warn_fail_count_ == 0:
				remote_file_list_ = client_.list(remote_dir_path_)
				for file_ in set(remote_file_list_) - set(keep_file_list_):
					if not file_.endswith('/'):
						print('deleting: {}'.format(file_))
						client_.clean(os.path.join(remote_dir_path_, file_))
	except WebDavException as exception_:
		print('WARNING: webdav exception: {}'.format(exception_))
		warn_fail_count_ += 1
	return warn_fail_count_

ARGS_ = None
def parse_args_():
	global ARGS_
	parser_ = argparse.ArgumentParser(description='Publish PDF (LaTeX) files via webdav.')
	parser_.add_argument('-f', required=True, help='Config file')
	parser_.add_argument('-c', action='store_true', help='Clean each repos?')
	parser_.add_argument('-b', help='Base directory for git pulls')
	parser_.add_argument('-s', action='store_true', help='Provide GPG signature for each file?')
	ARGS_ = parser_.parse_args()

def base_pulls_():
	with os.scandir(os.path.expanduser(ARGS_.b)) as ents_:
		for e_ in ents_:
			if e_.is_dir() and e_.name not in ['.', '..'] and os.path.exists(os.path.join(e_.path, '.git')):
				print('====> pulling {}'.format(e_.name))
				subprocess.run(['git', 'pull'], cwd=e_.path)

# Get all the account information, including which files to build and
# upload, from the configuration file, passed as first argument. Other
# arguments are ignored.
parse_args_()
if ARGS_.b is not None: base_pulls_()
account_list_ = yaml.load(slurp_file_(ARGS_.f), Loader=yaml.FullLoader)
warn_fail_count_ = 0
for account_ in account_list_:
	warn_fail_count_ += process_account_(account_)
print('====> WARNINGS / FAILS: {}'.format(warn_fail_count_))
